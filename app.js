var createError = require('http-errors');
var express = require('express');
const session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var flash = require('connect-flash');
var methodOverride = require('method-override')

const db_url = process.env.DB_URI || "mongodb://admin:Indonesia92@belajar-mongo-shard-00-00-8vwyw.mongodb.net:27017,belajar-mongo-shard-00-01-8vwyw.mongodb.net:27017,belajar-mongo-shard-00-02-8vwyw.mongodb.net:27017/genshin_db?ssl=true&replicaSet=belajar-mongo-shard-0&authSource=admin&retryWrites=true&w=majority";

// import mongoose
var mongoose = require('mongoose')
mongoose.connect(db_url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false
});

var indexRouter = require('./routes/index');
var adminRouter = require('./routes/admin');
var apiRouter = require('./routes/api');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(methodOverride('_method'));
app.use(flash());
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 3600000 }
}))

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/', express.static(path.join(__dirname, 'public')));
app.use('/sb-admin-2', express.static(path.join(__dirname, 'node_modules/startbootstrap-sb-admin-2')))

app.use('/', indexRouter);
app.use('/admin', adminRouter);
app.use('/genshin-api/v1', apiRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  // res.status(404).json({message: "general error"})
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
