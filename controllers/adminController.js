// load mongoose model
const Users = require('../models/users')
const Weapons = require('../models/weapons')
const Artifacts = require('../models/artifacts')
const Characters = require('../models/characters')
const Roles = require('../models/roles')

// utils
const bcrypt = require('bcrypt')
// const { isArray } = require('jquery')

module.exports = {
    viewSignin: async (req, res) => {
        try {
            const alertMessage = req.flash('alertMessage');
            const alertStatus = req.flash('alertStatus');
            const alert = { message: alertMessage, status: alertStatus }
            if(req.session.user == null || req.session.user == undefined) {
                res.render('index', {
                    alert,
                    title: "Genshin API | Login"
                })
            } else {
                res.redirect('/admin/dashboard');
            }
        } catch (error) {
            res.redirect('/admin/signin');
        }
    },

    actionSignin : async (req, res) => {
        try {
            const { username, password } = req.body;
            const user = await Users.findOne({ username: username });
            if (!user) {
                req.flash('alertMessage', 'User yang anda masukan tidak ada!!');
                req.flash('alertStatus', 'danger')
                res.redirect('/admin/signin');
            }
            const isPasswordMatch = await bcrypt.compare(password, user.password);
            if(!isPasswordMatch) {
                req.flash('alertMessage', 'Password Salah!!');
                req.flash('alertStatus', 'danger')
                res.redirect('/admin/signin');
            }
            req.session.user = {
                id: user.id,
                username: user.username
            }
            res.redirect('/admin/dashboard')
        } catch (error) {
            res.redirect('/admin/signin');
        }
    },
    
    actionLogout :  (req, res) => {
        req.session.destroy();
        res.redirect('/admin/signin');
    },

    viewDashboard: async (req, res) => {
        const weapon = await Weapons.find();
        const artifact = await Artifacts.find();
        const character = await Characters.find();

        const patch_info = "Patch v1.2"

        try {
            res.render('admin/dashboard/view_dashboard', { 
                title: "Genshin API | Dashboard", 
                user: req.session.user,
                weapon,
                artifact,
                character,
                patch_info
            });
            
        } catch (error) {
            res.redirect('/admin/dashboard')
        }
    },

    viewWeapon: async (req, res) => {
        try {
        const weapon = await Weapons.find();
        const alertMessage = req.flash('alertMessage');
        const alertStatus = req.flash('alertStatus');
        const alert = { message: alertMessage, status: alertStatus }
        res.render('admin/weapon/view_weapon', { weapon, alert, title: "Genshin API | Weapons", user: req.session.user});
        } catch(error) {
            res.redirect('/admin/weapon');
        }
    },

    editWeapon: async (req, res) => {
        try {
        const { id, name, rarity, mainstat, substat,  imageUrl, passive} = req.body;
        const weapon = await Weapons.findOne({ _id:id });
        weapon.name = name;
        weapon.rarity = rarity;
        weapon.main_stat = mainstat;
        weapon.sub_stat = substat;
        weapon.imageUrl = imageUrl;
        weapon.passive = passive;
        await weapon.save();
            req.flash('alertMessage', 'Success Edit Weapon');
            req.flash('alertStatus', 'success')
            res.redirect('/admin/weapon');
        } catch (error) {
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/weapon');
        }
    },

    addWeapon: async (req, res) => {
        try {
            const { name, type, rarity, main_stat, sub_stat,  imageUrl, passive } = req.body;
            await Weapons.create({ name, type, rarity, main_stat, sub_stat,  imageUrl, passive });
            req.flash('alertMessage', 'Success Add Weapon');
            req.flash('alertStatus', 'success')
            res.redirect('/admin/weapon');
        } catch (error){
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/weapon');
        }
    },

    deleteWeapon: async (req, res) => {
        try {
            const { id } = req.params;
            const weapon = await Weapons.findOne({ _id: id });
            await weapon.remove();
            req.flash('alertMessage', 'Success Delete Weapon');
            req.flash('alertStatus', 'success')
            res.redirect('/admin/weapon');
        } catch (error) {
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/weapon');
        }

    },

    viewArtifact: async (req, res) => {
        try {
        const artifact = await Artifacts.find();
        const alertMessage = req.flash('alertMessage');
        const alertStatus = req.flash('alertStatus');
        const alert = { message: alertMessage, status: alertStatus }
        res.render('admin/artifact/view_artifact', { artifact, alert, title: "Genshin API | Artifacts", user: req.session.user});
        } catch(error) {
            res.redirect('/admin/artifact');
        }
    },

    editArtifact: async (req, res) => {
        try {
        const { id, name, rarity, desc2, desc4,  imageUrl } = req.body;
        const artifact = await Artifacts.findOne({ _id:id });
        artifact.name = name;
        artifact.rarity = rarity;
        artifact.desc_2 = desc2;
        artifact.desc_4 = desc4;
        artifact.imageUrl = imageUrl;
        
        await artifact.save();
            req.flash('alertMessage', 'Success Edit Artifact');
            req.flash('alertStatus', 'success')
            res.redirect('/admin/artifact');
        } catch (error) {
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/artifact');
        }
    },

    addArtifact: async (req, res) => {
        try {
            const { name, rarity, desc_2, desc_4,  imageUrl } = req.body;
            await Artifacts.create({ name, rarity, desc_2, desc_4,  imageUrl });
            req.flash('alertMessage', 'Success Add Artifact');
            req.flash('alertStatus', 'success')
            res.redirect('/admin/artifact');
        } catch (error){
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/artifact');
        }
    },

    deleteArtifact: async (req, res) => {
        try {
            const { id } = req.params;
            const artifact = await Artifacts.findOne({ _id: id });
            await artifact.remove();
            req.flash('alertMessage', 'Success Delete Artifact');
            req.flash('alertStatus', 'success')
            res.redirect('/admin/artifact');
        } catch (error) {
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/artifact');
        }

    },

    showCharacterItem: async (req, res) => {
        try {
            const { id } = req.params;
            const character = await Characters.find({ _id: id })
                .populate({ path: 'weapons', select:'id imageUrl rarity main_stat sub_stat passive'})
                .populate({ path: 'artifacts', select: 'id name imageUrl rarity desc_2 desc_4'});
            const alertMessage = req.flash('alertMessage');
            const alertStatus = req.flash('alertStatus');
            const alert = { message: alertMessage, status: alertStatus }
            res.render('admin/item/view_item', {title: "Genshin API | Show Character Details", user: req.session.user, alert, character, action: 'show image'});
            
        } catch (error) {
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/item');
        }
    },

    viewCharacter: async (req, res) => {
        try {
            const character = await Characters.find()
            .populate({ path: 'weapons', select:'-_id name'})
            .populate({ path: 'artifacts', select: '-_id name'});

        const roles = ['main dps', 'sub dps', 'utilities', 'support'];
        const stats = ["crit rate", "crit dmg", "atk", "atk%", "healing bonus", "hp", "hp%","def", "def%", "energy recharge", "elemental mastery"]
        const weapon = await Weapons.find();
        const artifact = await Artifacts.find();
        // const roles = await Roles.find();

        const alertMessage = req.flash('alertMessage');
        const alertStatus = req.flash('alertStatus');
        const alert = { message: alertMessage, status: alertStatus }
        // console.log("Character : ", character)
        // console.log("weapon : ", weapon)
        // console.log("artifact : ", artifact)
        res.render('admin/character/view_character', { stats, weapon, artifact, roles, character, alert, title: "Genshin API | Characters", user: req.session.user});
        } catch(error) {
            res.redirect('/admin/character');
        }
    },

    addCharacter: async (req, res) => {
        try {
            const { name, weapon, description, imageUrl, gender, birthday, rarity, vision, roles, weapons, artifactsBuild, statsPriority, set} = req.body;

            let arr = []
            arte = Array.isArray(artifactsBuild) == true ? artifactsBuild : [artifactsBuild] 
            label = Array.isArray(set) == true ? set : [set] 

            arr = arte.map((arteId, setDesc) => ({
                artifactId: arteId,
                sets: label[setDesc]
            }));
            // console.log("percobaan", arr)
            await Characters.create({ name, weapon, description, roles, gender, rarity, statsPriority, birthday, vision, weapons, artifacts: arr , imageUrl });
            req.flash('alertMessage', 'Success Add Character');
            req.flash('alertStatus', 'success')
            res.redirect('/admin/character');
        } catch (error){
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/character');
        }
    },

    deleteCharacter: async (req, res) => {
        try {
            const { id } = req.params;
            const character = await Characters.findOne({ _id: id });
            await character.remove();
            req.flash('alertMessage', 'Success Delete Character');
            req.flash('alertStatus', 'success')
            res.redirect('/admin/character');
        } catch (error) {
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/character');
        }

    },

    editCharacter: async (req, res) => {
        try {
        const { id, name, weapon, description, imageUrl, gender, birthday, rarity, vision, roles, weapons, artifactsBuild, statsPriority, set } = req.body;
        let arr = []
        arte = Array.isArray(artifactsBuild) == true ? artifactsBuild : [artifactsBuild] 
        label = Array.isArray(set) == true ? set : [set] 

        arr = arte.map((arteId, setDesc) => ({
            artifactId: arteId,
            sets: label[setDesc]
        }));
        const character = await Characters.findOne({ _id:id });
        character.name = name;
        character.rarity = rarity;
        character.weapon = weapon;
        character.description = description;
        character.gender = gender;
        character.birthday = birthday;
        character.vision = vision;
        character.roles = roles;
        character.weapons = weapons;
        character.artifacts = arr;
        character.statsPriority = statsPriority;
        character.imageUrl = imageUrl;

        await character.save();
            req.flash('alertMessage', 'Success Edit Character');
            req.flash('alertStatus', 'success')
            res.redirect('/admin/character');
        } catch (error) {
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/character');
        }
    },

    viewDetailCharacter: async (req, res) => {
        try {
            const { id } = req.params;
            const alertMessage = req.flash('alertMessage');
            const alertStatus = req.flash('alertStatus');
            const alert = { message: alertMessage, status: alertStatus }
            const character = await Characters.find({_id : id})
            .populate({ path: 'weapons', select:'-_id name imageUrl rarity main_stat sub_stat passive'})
            .populate({ path: 'artifacts.artifactId', select: '-_id name imageUrl rarity desc_2 desc_4' });
            console.log("spesifikz", character[0].artifacts);
            res.render('admin/character/details/details', {title: "Character | Details", user: req.session.user, alert, character});
        } catch (error) {
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/character');
        }
    },

    viewEditCharacter: async (req, res) => {
        try {
            const { id } = req.params;
            const alertMessage = req.flash('alertMessage');
            const alertStatus = req.flash('alertStatus');
            const alert = { message: alertMessage, status: alertStatus }
            const character = await Characters.find({_id : id})
            .populate({ path: 'weapons', select:'-_id name imageUrl rarity main_stat sub_stat passive'})
            .populate({ path: 'artifacts.artifactId', select: '-_id name imageUrl rarity desc_2 desc_4'});
            const weapon = await Weapons.find();
            const artifact = await Artifacts.find();
            const charaRole = character[0].roles
            const roles = ['main dps', 'sub dps', 'utilities', 'support'];
            const stats = ["crit rate", "crit dmg", "atk", "atk%", "healing bonus", "hp", "hp%","def", "def%", "energy recharge", "elemental mastery"]
            console.log("spesifik", character[0].weapons[0].imageUrl);
            res.render('admin/character/edit/edit_character', {title: "Character | Edit", user: req.session.user, alert, character, roles, charaRole, stats, weapon, artifact});
        } catch (error) {
            req.flash('alertMessage', `${error.message}`);
            req.flash('alertStatus', 'danger')
            res.redirect('/admin/character');
        }
    },
}