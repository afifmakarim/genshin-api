const Weapons = require('../models/weapons')
const Artifacts = require('../models/artifacts')
const Characters = require('../models/characters')


module.exports = {
    allCharacter: async (req, res) => {
        try {
            const character = await Characters.find().select('name weapon description gender birthday rarity vision imageUrl')
    
            let returnedCharacter = [];
            for (let i = 0; i < character.length; i++) {
                returnedCharacter.push(character[i].transform());
            }
            res.status(200).json({ 
                responseCode: "00",
                status : "success",
                data: returnedCharacter
            });
        } catch (error) {
            console.log(error)
            res.status(500).json({message: "internal server error"})
        }
    },

    characterByVision: async (req, res) => {
        const { vision } = req.params;

        var element = ["cryo", "pyro", "dendro", "hydro", "geo", "electro", "anemo"];
        var n = element.includes(vision.toLowerCase());
        
        try {
            const character = await Characters.find({ vision: vision.toLowerCase() })
            .select('name roles weapon description gender birthday rarity vision imageUrl') 

            let returnedCharacter = [];
            for (let i = 0; i < character.length; i++) {
                returnedCharacter.push(character[i].transform());
            }

            if (n == false){
                res.status(500).json({message: "invalid paramater"})
            }

            if (character.length == 0) {
                res.status(404).json({message: vision.toLowerCase() + " element not found"})
            }

            res.status(200).json({ 
                responseCode: "00",
                status : "success",
                data: returnedCharacter
            });
        } catch (error) {
            console.log(error)
            res.status(500).json({message: "internal server error"})
        }
    },

    characterDetail: async (req, res) => {
        const { id } = req.params;
        try {
            const character = await Characters.findOne({ _id: id })
            .populate({ path: 'weapons', select:'-_id name imageUrl rarity main_stat sub_stat passive'})
            .populate({ path: 'artifacts.artifactId', select: '-_id name imageUrl rarity desc_2 desc_4'});

            res.status(200).json({ 
                responseCode: "00",
                status : "success",
                data: character.transform()
            });
        } catch (error) {
            console.log(error)
            res.status(500).json({message: "Character not found with id " + id})
        }
    },

    weaponPage: async (req, res) => {
        try {
            const weapon = await Weapons.find();
            res.status(200).json({ 
                responseCode: "00",
                status : "success",
                data: weapon
            });
        } catch (error) {
            console.log(error)
            res.status(500).json({message: "internal server error"})
        }
    },

    weaponDetail: async (req, res) => {
        const { id } = req.params;
        try {
            const weapon = await Weapons.findOne({ _id: id })

            res.status(200).json({ 
                responseCode: "00",
                status : "success",
                data: weapon.transform()
            });
        } catch (error) {
            console.log(error)
            res.status(500).json({message: "Weapon not found with id " + id})
        }
    },

    artifactPage: async (req, res) => {
        try {
            const artifact = await Artifacts.find();
            res.status(200).json({ 
                responseCode: "00",
                status : "success",
                data: artifact
            });
        } catch (error) {
            console.log(error)
            res.status(500).json({message: "internal server error"})
        }
    },

    artifactDetail: async (req, res) => {
        const { id } = req.params;
        try {
            const artifact = await Artifacts.findOne({ _id: id })

            res.status(200).json({ 
                responseCode: "00",
                status : "success",
                data: artifact.transform()
            });
        } catch (error) {
            console.log(error)
            res.status(500).json({message: "Artifact not found with id " + id})
        }
    }
}