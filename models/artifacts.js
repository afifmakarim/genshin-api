const mongoose = require('mongoose')

const artifactsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    },
    rarity: {
        type: String,
        required: true
    },
    desc_2: {
        type: String,
        required: true
    },
    desc_4: {
        type: String,
        required: true
    }
})

artifactsSchema.method('transform', function() {
    var obj = this.toObject();
 
    //Rename fields
    obj.id = obj._id;
    delete obj._id;
    delete obj.__v;

 
    return obj;
});

module.exports = mongoose.model('Artifacts', artifactsSchema)