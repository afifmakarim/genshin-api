const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema;

const charactersSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    weapon: {
        type: String,
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    birthday: {
        type: String,
        required: true
    },
    rarity: {
        type: String,
        required: true
    },
    vision: {
        type: String,
        required: true
    },
    roles: [{
        type: String,
        required: true
    }],
    weapons: [{
        type: ObjectId,
        ref: 'Weapons'
    }],
    artifacts: [{
        artifactId: {
            type: ObjectId,
            ref: 'Artifacts',
            required: true
        },
        sets: {
            type: String,
            required: true
        }
    }],
    statsPriority: [{
        type: String,
        required: true
    }],
    
})

// charactersSchema.set("toJSON", {
//     virtuals: true,
// });

charactersSchema.method('transform', function() {
    var obj = this.toObject();
 
    //Rename fields
    obj.id = obj._id;
    delete obj._id;
    delete obj.__v;

 
    return obj;
    
});

module.exports = mongoose.model('Characters', charactersSchema)