const mongoose = require('mongoose')

const rolesSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
})

module.exports = mongoose.model('Roles', rolesSchema)
