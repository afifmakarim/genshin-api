const mongoose = require('mongoose')

const weaponsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    },
    rarity: {
        type: String,
        required: true
    },
    main_stat: {
        type: String,
        required: true
    },
    sub_stat: {
        type: String,
        required: true
    },
    passive: {
        type: String,
        required: true
    }
})

weaponsSchema.method('transform', function() {
    var obj = this.toObject();
 
    //Rename fields
    obj.id = obj._id;
    delete obj._id;
    delete obj.__v;

 
    return obj;
});

module.exports = mongoose.model('Weapons', weaponsSchema)