package main

import (
"bytes" 
"crypto/aes" 
"crypto/cipher" 
"crypto/md5" 
"crypto/rand" 
b64 "encoding/base64" 
"encoding/hex" 
"encoding/json" 
"errors" 
"fmt" 
"io"
)

var prefixLa = []byte("LinkAja_")
var secret = "partner_client_secret"
var request = map[string]interface{}{
		"foo": "bar", 
		"baz": map[string]interface{}{ 
		"a": "b",
		}, 
}

var encrypted = []byte("TGlua0FqYV8JqOdP0zP5wYz8T8pl2wLvXvJZ7bkeIObE96RHHifIV8CehpRrzHWYUUEkeiGDPUrfZ/bvAn9O5/U=")

func main() {

payload, err := json.Marshal(request) if err != nil { panic(err) }

cp, err := encryptGCM(payload, secret)

if err != nil { panic(err) }

fmt.Println("encrypted:", string(cp))

}

func encryptGCM(text []byte, passphrase string) (res []byte, err error) { 

var key string md := md5.New() md.Write([]byte(passphrase)) 
key = hex.EncodeToString(md.Sum(nil))

block, _ := aes.NewCipher([]byte(key)) 
gcm, err := cipher.NewGCM(block) 
if err != nil {
return 
} nonce := make([]byte, gcm.NonceSize()) if _, err = io.ReadFull(rand.Reader, nonce); err != nil {

return }

ciphertext := gcm.Seal(nonce, nonce, text, nil) res = []byte(b64.StdEncoding.EncodeToString(append(prefixLa, ciphertext...)))

return

}