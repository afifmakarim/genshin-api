### Installation

genshin-api requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ npm install
$ npm start
```

[Optional] Change Dashboard Theme

```sh
$ npm install -g sass
$ cd public/stylesheets
$ sass custom.scss custom.css
```