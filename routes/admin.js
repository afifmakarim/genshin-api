var express = require('express');
var router = express.Router();

const adminController = require('../controllers/adminController')
const auth = require('../middleware/auth');

/* auth routes */
router.get('/signin', adminController.viewSignin);
router.post('/signin', adminController.actionSignin);
router.use(auth);
router.get('/logout', adminController.actionLogout);
router.get('/dashboard', adminController.viewDashboard);
/* weapon routes */
router.get('/weapon', adminController.viewWeapon);
router.post('/weapon', adminController.addWeapon);
router.put('/weapon', adminController.editWeapon);
router.delete('/weapon/:id', adminController.deleteWeapon);

/* artifact routes */
router.get('/artifact', adminController.viewArtifact);
router.post('/artifact', adminController.addArtifact);
router.put('/artifact', adminController.editArtifact);
router.delete('/artifact/:id', adminController.deleteArtifact);

/* character routes */
router.get('/character/details/:id', adminController.viewDetailCharacter);
router.get('/character/edit/:id', adminController.viewEditCharacter);
router.get('/character', adminController.viewCharacter);
router.post('/character', adminController.addCharacter);
router.put('/character', adminController.editCharacter);
router.delete('/character/:id', adminController.deleteCharacter);

module.exports = router;