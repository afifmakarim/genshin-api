var express = require('express');
var router = express.Router();
const apiController = require('../controllers/apiController')
const patch_version = process.env.PATCH_VER || "v1.3";

/* GET */
router.get('/', function(req, res, next) {
  res.status(200).json({message: "genshin-api version 1.0", patch: patch_version})
});

// Characters
router.get('/characters', apiController.allCharacter);
router.get('/character/:id', apiController.characterDetail);
router.get('/characters/:vision', apiController.characterByVision);

// Weapons
router.get('/weapons', apiController.weaponPage);
router.get('/weapon/:id', apiController.weaponDetail);

// Artifacts
router.get('/artifacts', apiController.artifactPage);
router.get('/artifact/:id', apiController.artifactDetail);


module.exports = router;